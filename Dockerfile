#FROM openjdk:8-jdk-alpine
#VOLUME /tmp
#COPY target/frontend-0.0.1-SNAPSHOT.jar app.jar
#ENV JAVA_OPTS=""
#ENTRYPOINT exec java -Dserver.port=8081 -jar /app.jar
FROM gcr.io/google_appengine/jetty

ENV JAVA_OPTS="-Djetty.http.port=8081"
ADD target/frontend-0.0.1-SNAPSHOT.war $JETTY_BASE/webapps/root.war
ADD . /app
RUN chown jetty:jetty $JETTY_BASE/webapps/root.war